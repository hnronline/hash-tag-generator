from tag_generator import TagGenerator
from tags_process import TagsProcessor

import time


start_time = int(round(time.time() * 1000))

print('Test img url: http://www.chowrangi.pk/wp-content/uploads/2016/04/fashion-in-pakistan.jpg')
print(' ')
imgPath = str(input('Enter image url: '))

print(' ')

tg = TagGenerator()

labels = tg.find_tags(imgPath)

tp = TagsProcessor()

tp.start_thread_process(labels, start_time)
