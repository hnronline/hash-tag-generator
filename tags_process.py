import requests
import operator
from threading import Thread
import time


class TagsProcessor:

    done_count = 0
    dictTags = {}

    def get_url(self, tag):
        return "http://instagram.com/explore/tags/" + tag + "/?__a=1"

    def get_ig_page(self, url, session=None):
        print("Connecting to: " + url)
        session = session or requests.Session()

        r = session.get(url)
        r_code = r.status_code
        print("Connected to " + url + " : " + str(r_code))
        if r.status_code == requests.codes.ok:
            # the code is 200 or valid
            return r
        else:
            return None

    def threaded_tag_process(self, thread_pos_args, tagItem, total, start_time):
        print("Starting threads no: " + str(thread_pos_args))
        data_dict = self.get_ig_page(self.get_url(tagItem))
        if data_dict is not None:
            data_dict = data_dict.json()

            data = data_dict.get('graphql', None)

            # hashtags
            hashtags = data.get('hashtag', None)

            # edge_hashtag_to_media
            edge_hashtag_to_media = hashtags.get('edge_hashtag_to_media', None)

            # edges
            edges = edge_hashtag_to_media.get('edges', None)
            # print(str(edges).replace("'", "\""))

            for edge in edges:
                nodes = edge.get('node')
                # print(str(nodes).replace("'", "\""))

                en = nodes.get('edge_media_to_caption')
                likes_count = int(nodes.get('edge_liked_by').get('count'))
                # print(str(en).replace("'", "\""))
                seds = en.get('edges')
                # print(str(seds).replace("'", "\""))

                for sed in seds:
                    # print(str(sed).replace("'", "\""))

                    ins_node = sed.get('node')
                    text = ins_node.get('text')

                    # print(text)

                    strs = text.split(' ')

                    for strr in strs:
                        if strr.startswith('#'):
                            str_lower = strr.lower()
                            if str_lower in self.dictTags:
                                self.dictTags[str_lower] = self.dictTags[str_lower] + likes_count + 1
                            else:
                                self.dictTags[str_lower] = likes_count + 1

        self.done_count = self.done_count + 1
        print("Threads done: " + str(self.done_count))

        if self.done_count == total:
            sorted_dict = sorted(self.dictTags.items(), key=operator.itemgetter(1), reverse=True)
            print(sorted_dict)

            end_time = int(round(time.time() * 1000))
            time_took = end_time - start_time
            print("Count: " + str(len(sorted_dict)))
            print("Operation took:" + str(time_took) + "ms")

    def start_thread_process(self, tags_array, start_time):
        thread_pos = 0
        for tagItem in tags_array:
            thread_pos = thread_pos + 1
            thread = Thread(target=self.threaded_tag_process, args=[thread_pos, tagItem, len(tags_array), start_time])
            thread.start()
